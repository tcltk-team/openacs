#!/bin/sh
# These variables can be customized to change OpenACS AOLserver settings.
# More changes can be done by modifying the /etc/openacs/config.tcl Tcl script.
#
AOL_USER=www-data
AOL_GROUP=www-data
AOL_ADDRESS=127.0.0.1
AOL_PORT=8000
RUN_DAEMON=yes
